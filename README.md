# e-bird.inc Document(Public)
e-bird社内の制作・開発向けドキュメント（公開可能なもののみ）をまとめたリポジトリです。


## ドキュメント一覧

## ルール

* [Gitリポジトリ作成について](https://bitbucket.org/ebird-inc/document-public/src/HEAD/rule/git.md?at=master)

### セットアップガイド・チュートリアル

* [ソフトウェア・基本ツールのセットアップ](https://bitbucket.org/ebird-inc/document-public/src/HEAD/learning/setup.md?at=master)
* [フロントエンドに必要なツールセットアップ](https://bitbucket.org/ebird-inc/document-public/src/HEAD/learning/environment.md?at=master)
* [Sassについて](https://bitbucket.org/ebird-inc/document-public/src/HEAD/learning/sass.md?at=master)
* [HTMLを初める前に](https://bitbucket.org/ebird-inc/document-public/src/HEAD/learning/html.md?at=master)
* [Gulpのインストール・セットアップガイド](https://bitbucket.org/ebird-inc/document-public/src/HEAD/tool/gulp/guide.md?at=master)

### ツール別ガイド

* [WordPress:テスト環境→本番環境への移行手順](https://bitbucket.org/ebird-inc/document-public/src/HEAD/tool/wordpress/database.md?at=master)
* [WordPress:コーディングガイド](https://bitbucket.org/ebird-inc/document-public/src/HEAD/tool/wordpress/codingguide.md?at=master)
* [WordPress:管理画面マニュアル※ダウンロードが必要](https://bitbucket.org/ebird-inc/document-public/src/HEAD/tool/wordpress/manual/?at=master)

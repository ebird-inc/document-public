# Development environment
* 2015.12.23 update
* 記述者: kgsi

このドキュメントはnode.jsやRubyなど、フロントエンド開発に必要なモジュール、プラグインの
セットアップドキュメントです。


## About
セットアップで導入するツールは以下の通りです

* [HomeBrew](http://brew.sh/index_ja.html)
* [Node.js](https://nodejs.org/en/)
* [gulp.js](http://gulpjs.com/)
* Ruby(version check)
* xCode Comandline tool
* [Bower](http://bower.io/)
* [Sass](http://sass-lang.com/)
* [fish(Command line shel)](http://fishshell.com/)


## Setup
Terminal.appを起動して下記コマンドを実行していってください。

### Install fish 
[Installer setup](http://fishshell.com/)

### Install Node.js 
[Installer setup](https://nodejs.org/en/)

### HomeBrew install
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    brew doctor // errorがあった場合は修正する

### Install Bower
    npm install -g bower

### Ruby version check
    ruby -v

### Install XCode Commandline tool 
    xcode-select --install

### Install Sass 
    sudo gem install sass

### Install Gulp 
    sudo npm i -g gulp

## Reference article

* [YosemiteでのXcodeのコマンドラインツールのインストール方法](http://taithon.hatenablog.com/entry/2015/04/23/Yosemite%E3%81%A7%E3%81%AEXcode%E3%81%AE%E3%82%B3%E3%83%9E%E3%83%B3%E3%83%89%E3%83%A9%E3%82%A4%E3%83%B3%E3%83%84%E3%83%BC%E3%83%AB%E3%81%AE%E3%82%A4%E3%83%B3%E3%82%B9%E3%83%88%E3%83%BC%E3%83%AB%E6%96%B9)
* [HomeBrew](http://brew.sh/index_ja.html)
* [Node.jsとgulpをインストールして使うまでの入門記事 Win/Mac対応](http://commte.net/blog/archives/5283)
* [初心者向け：nodebrewでNode.jsをバージョン管理し、環境を整える（MacOSX）](http://tipsbear.com/homebrew-nodebrew-nodejs/)
* [OSXでfish shellを使う](http://qiita.com/osakanafish/items/0852327c1450123f1287)


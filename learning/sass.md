# Sassについて
* 2015.12.18 update
* 記述者: kgsi

このドキュメントはSass（CSS）を使うための環境を整え、操作方法を簡単に学ぶチュートリアルガイドです。

## About
e-birdでは基本的に理由がなければSCSS(SASS)を利用しています。
CSSと比べて良い点は以下のとおり。

* ネストできるので可視性に優れる
* プログラムライクなcompileが可能（所謂variable,functionの定義が可能）
* ネット上の情報が豊富

メリットが多いため、特に理由のない場合や小規模なサイト制作を除き、
SASSでCSS作成を行います。


## Setup
### 環境セットアップ
* [koara](http://koala-app.com/)
* [Sass](http://websae.net/sass-compile-20140904/)
* gulp（gulpについては別途説明します）

## compile
#### Lesson
style.scssをstyle.cssにコンパイルしてみよう

## variable
Sassはプログラムと同じく変数が定義できる。

	// variable
	$width:500px;
	$color:#aa443f;

	// style
	.elm{
		width:$width;
		color:$color;
	}

#### Lesson
変数を定義してコンパイルしてみよう。

## nest
SASSではスタイルをネストを用いて簡潔に記述できる。

	// before
	#elem{
		background-color:#000;
		.content{
			background-color:#aaa;
		}
	}

	// after
	#elem {
	  background-color: black;
	}
	#elem .content {
	  background-color: #aaaaaa;
	  color: black;
	}

#### Lesson
* CSSの継承スタイルをネストしよう
* ネームスペースをネストしてみよう


## mixin
mixinというものを定義して、同じスタイルを使いまわせる。

	// before
	@mixin text-base {
		font:{
			family: Arial;
			size: 20px;
			weight: bold;
		}
		color: #ff0000;
	}
	.text{
		@include text-base;
		background-color:#fff;
	}
	
	// after
	.text {
	  font-family: Arial;
	  font-size: 20px;
	  font-weight: bold;
	  color: #ff0000;
	  background-color: #fff;
	}

#### Lesson
mixinを定義してincludeしてみよう


## import files
SASSではimport機能を使って外部ファイルを結合することができる。

    @import "base";
    @import "pages";

#### Lesson
style.scss、base.scss、pages.scssファイルを作成し、
style.scssに@importしてコンパイルし、style.cssを作成しよう

## Final Lesson
渡すHTMLファイルのCSSをSASS化してください。

## Practice
* インストラクターが作業する所をみる
* インストラクター作業しているところを見てチェックする


## Reference article

* [これからSassを始めたい人へ！導入手順をまとめてみた](http://liginc.co.jp/web/html-css/css/56599)
* [Mac 環境にSassをインストールする](http://book.scss.jp/code/c2/02.html)
* [CSSコーディングで泣かないためのSassの基礎知識と10の利点](http://www.atmarkit.co.jp/ait/articles/1402/17/news102.html)
# ソフトウェア・基本ツールのセットアップ
 
* 2018.01.15
* 記述者: kgsi

## はじめに
e-bird社内でコーディング、及び実装を行うために必要なツール、ソフトウェアのセットアップ指南書です。


## Adobe Creative Cloud
弊社ではAdobe Creative CloudでPhotoshopやIllustratorのインストールを行っています。
デザイナー・コーダーどちらも必要な場合が多いため、必要な場合は、担当者に必要な旨を伝え、
下記サイトから、Adobe Creative Cloudアプリをダウンロードしてください。作成してもらったアカウント設定後、利用できます。

[Adobe Creative Cloud](http://www.adobe.com/jp/creativecloud/catalog/desktop.html)


## Git
社内プロジェクトの大半はgitにて管理しています。
gitについて知らないという方は以下のドキュメントを参照し、基礎学習をしてください。

* [SourceTreeでGitを始めよう！](http://naichilab.blogspot.jp/2014/01/git-1sourcetreegitgit.html)
* [サルでもわかるGit入門](https://backlog.com/git-tutorial/ja/)


### Bitbucket
BtoBに特化したリモートリポジトリです。e-birdで利用しているリポジトリとなります。業務用メールでBitbucketのアカウントを新規作成し、作成したアカウント名をgit管理人（kgsi）に伝えてください。

[Bitbucket](https://bitbucket.org/)


### Sourcetree
ローカルリポジトリやリモートリポジトリをGUIで管理操作するアプリです。なお、Sourcetreeは無料ですが、アカウントの登録が必要です。

[Sourcetree](https://ja.atlassian.com/software/sourcetree)


### 使い方について
具体的な使い方の資料は以下になります。すでに知っている場合はスキップしてください

* [Gitクライアントツール[SourceTree]＆Bitbucketリモートリポジトリ作成](http://www.casleyconsulting.co.jp/blog-engineer/git/3808/)
* [【連載Git入門 第5回】SourceTreeでGitを始めよう！リモートリポジトリを使ってみよう](http://naichilab.blogspot.jp/2014/01/git-5sourcetreegit.html)


## Slack
弊社では社内チャットはSlackにて行っています。Slackは基本クローズドな招待制となります。

[e-bird Slack(招待制)](https://e-bird.slack.com/)

e-bird用メールの作成後、Inviteメールを送ります。手順に従って登録を済ませてください。
Slackの使い方を知らない場合は、以下のドキュメントを見て基礎学習をしてください。

* [Slack の使い方](https://get.slack.help/hc/ja/categories/200111606-Slack-%E3%81%AE%E4%BD%BF%E3%81%84%E6%96%B9)
* [基礎から応用までしっかり学ぶSlack入門 記事一覧](https://thinkit.co.jp/series/6318)


## フォント
弊社ではモリサワパスポート他、基本的な英語フォントの配布を行っています。デザイナーは特に必要になると思われるため、
インストールしたい場合は担当者（kgsi）に連絡をお願いします。

## MAMP
local環境にLAMP開発環境を構築するために、MAMPのセットアップを行ってください。

[MAMP](https://www.mamp.info/de/)


## FTPClient
FTPClientソフトはTransmitを使っています。
有料ソフトなので、購入申請を担当者にしてください。

[TRANSMIT5](https://panic.com/jp/transmit/)
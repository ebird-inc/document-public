# HTMLを初める前に
 
* 2016.12.18
* 記述者: kgsi

この記事はタイスタッフ向けに書いたHTMLの指南書です。  

## About HTML
 * 近年Web Developerに求められる事はますます多くなってきている。その中でも基本中の基本、最低限必要な技術はHTMLとCSSである。
 * HTMLはWebアプリやスマートフォンアプリにも使われる汎用性の高い技術。正確に、クオリティの高いファイルを安定して作成・量産できることがWeb Developerに必要。

## Skills that are required
日本向けサイトで、特にe-birdで制作する際に求められるHTMLのチェックポイントは以下のとおり。

### 1.Accuracy
マークアップ構造や、タグの閉じ忘れなどのチェックは必須。
納品前には以下のサイトでチェックをする。  
[Markup Validation Service](https://validator.w3.org/)

### 2.SEO
metaタグ、altタグへの情報入力は必ず行うこと。こちらもAccurayと同じサイトでチェックできる。
[Markup Validation Service](https://validator.w3.org/)

### 3.Associativity
他人に渡してもわかりやすく、読めるコードになっているかどうか。
インテンドや改行は正確にルールに従って行う。

### 4.Commonality
例えばheaderや、footerなど共通化できるところはなるべく共通化する。phpやssi等の技術を使うのが一般的。


##Production flow

1. デザイン受領
2. gitリポジトリの作成申請、Push 
3. 画像書き出し
4. HTML制作
5. SEO・マークアップチェック
6. 日本での確認
6. チェックバック対応
7. 最終確認
8. 作業完了


## Mark-up rules

HTMLにもっとも必要なのはマークアップのルール。

### Example

マークアップのルールは様々あります。

[Google -Style guide-](http://re-dzine.net/2012/05/google-htmlcss-style-guide/)

#### Link

* Apacheのサーバではindex.htmlの場合、ファイル名を省略しても表示されるので、URLのファイル名は省略する。
* パスは基本相対パスで書く(../../dir/)。パーツをincludeしたり、Wordpressといったシステムが入る場合は絶対相対パスで書く(/dir/)

#### Filename
単語はわかりやすいように。たとえば「h-l.png」といった
1文字省略だと分かりにくいので、「header-logo.png」など誰がみても分かる名前にするように。

#### External file
CSSやJSはできるだけhTMLに直に書かない。メンテナンス性がおちるため。

##Base Template

### HTML
基本、このHTMLを下地にコーディングをして欲しいです。

	<!DOCTYPE HTML>
	<!--[if lte IE 8 ]><html class="ie8 ie8L ie9L" lang="ja" xmlns:og="http://ogp.me/ns#"><![endif]-->
	<!--[if IE 9 ]><html class="ie9 ie9L" lang="ja" xmlns:og="http://ogp.me/ns#"><![endif]-->
	<!--[if (gt IE 9)|!(IE)]><!--><html class="noIE" lang="ja" xmlns:og="http://ogp.me/ns#"><!--<![endif]-->
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">
	<meta name="format-detection" content="telephone=no">
	<meta property="og:title" content="title">
	<meta property="og:type" content="website">
	<meta property="og:image" content="http://xxx.xxxxx.com/assets/images/commons/ogimage.png">
	<meta property="og:url" content="http://xxx.xxxxx.com/">
	<meta property="og:site_name" content="title">
	<meta name="description" content="xxx">
	<link rel="canonical" href="http://xxx.xxxxx.com/" />
	<link rel="shortcut icon" href="/favicon.ico" />
	<link rel='stylesheet' href="/assets/css/style.css">
	<script src="/assets/js/vendor/libs.js"></script>
	<script src="/assets/js/app.js"></script>
	<!--[if lt IE 9]>
	<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<title>title</title>
	</head>
	<body class="top">
	<div class="wrapper">
	</div>
	<!-- //wrapper -->
	</body>
	</html>
	
### Directory

基本的に以下の構造で作成する。理由はファイルやフォルダでルートディレクトリをなるべく汚染させないため。

    index.html
    assets
      └ sass   
      └ include
      └ js
        └ src
        └ vendor
      └ images


#リンクのクリックやダウンロードを「イベント」として計測
* 2015.07.16 update
* 記述者: lee

##イベントトラッキング編
###実装例
[リバリュー](http://revalue.jp/)←リファラーさせたくないのでクリックしないでくださいw

###ソースコード 

    onclick="_gaq.push(['_trackEvent','AB_PC','linkClick','copyA']);"
    onclick="_gaq.push(['_trackEvent','AB_PC','linkClick','copyA']);"

###JavaScriptのonClickで_trackEventの引数に値を設定

####_gaq.pushメソッドは、Googleアナリティクスで用意されてるもの
    例）
    _gaq.push(['_trackEvent', '第1引数', '第2引数', '第3引数', 第4引数, 第5引数])

下記は、必須要素

* 第1引数：カテゴリ　　⇒　グループの名前
* 第2引数：アクション　⇒　ユーザーの操作　例：click
* 第3引数：ラベル名　　⇒　分析を識別する文字列
「イベントカテゴリ」「イベントアクション」「イベントラベル」の3つの軸で「イベント」を集計・分析

下記は用途に応じて

* 第4引数：値　　　　　⇒　数値（1クリックの価値）
* 第5引数：値　　　　　⇒　ブーリアン（true or false）デフォルトではtrue　※イベントトラッキングの処理と直帰率のデータに関する項目

####必須項目のみ記述した例

    _gaq.push(['_trackEvent', 'category', 'action']);

####ぜんぶいりで記述した例
間にあるオプションの1つを指定しなかった場合は、そのオプションの部分の前後のカンマを残しておくこと   

    _gaq.push(['_trackEvent', 'category', 'action', 'label', , true]);

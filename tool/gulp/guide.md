#Gulp.js Install & Setup Guide
* 2015.08.11 update
* 記述者: kgsi

e-birdで使用しているGulpプロジェクトを使った制作環境構築を説明します。  
このドキュメントでは初期設定から実行コマンドまでを解説しています。

##Gulp.jsとは
Terminalから実行するタスクランナーツールです。
JSや画像圧縮、SASSコンパイル、ローカルサーバ構築、リアルタイムプレビュー...など
様々な処理を自動化します。e-birdではGulp.jsをメインに使っています。
Gulp.jsの詳細については下記URLなどを参照してください。

* [5分で導入！ タスクランナーGulpでWeb制作を効率化しよう](http://ics-web.jp/lab/archives/3290)
* [Gulp.js入門](http://qiita.com/yoshinariiii/items/8340286ec6f3ed6ff888)

##操作の前に
Gulp.jsの実行にはTerminalを使うことが必要不可欠です。  
また、e-birdで使用してるGulpプロジェクトはGitの他、EJS・SASSを採用しているため、それぞれの知識が必要です。下記サイトなどで概要を学習してください。

* [今さら聞けない！ターミナルの使い方【初心者向け】](http://techacademy.jp/magazine/5155)
* [CSSの常識が変わる！「Compass」の基礎から応用まで！](http://liginc.co.jp/designer/archives/11623)
* [EJSテンプレートエンジンを使おう!](http://libro.tuyano.com/index3?id=1135003)

##使用Gulpプロジェクト
e-birdで使用してるプロジェクトファイルは以下です。

###[gulp-initial](https://github.com/kgsi/gulp-initial)###

仕様やコマンドは上記URLのREADMEから確認して下さい。
なお、用途や環境によってはこのプロジェクトでは使いにくい場合もあると思われますので、その場合はカスタム、または別のプロジェクトを使用、新規で作成するなどして対応してください。


##Gulp実行のために必要なツール
 * [Node.js](https://nodejs.org/)
 * [Gulp.js](http://gulpjs.com/)
 * Ruby
 * Sass
 * [Compass](http://compass-style.org/)

これらのセットアップは各々の環境によって異なるため、下記サイトを参考にして
環境をセットアップしてください。

 * [Node.jsとgulpをインストールして使うまでの入門記事 Win/Mac対応](http://commte.net/blog/archives/5283)
 * [Gulp.js入門 – コーディングを10倍速くする環境を作る方法まとめ](http://liginc.co.jp/web/tutorial/117900)

##セットアップ手順
上記の必須ツールがインストールされている前提で
セットアップ手順を進めます。

###1.npm, gem, sass, compassのアップデート
パッケージのアップデートを行い、環境を最新の状態にする。

####node.js(npm)のupldate
	$ sudo npm update -g npm
	$ sudo npm cache clean -f
	$ sudo npm install -g n
	$ sudo n stable

####Ruby(gem)のupldate
	$ sudo gem update --system 
	$ sudo gem update sass
	$ sudo gem update compass

###2.Gulpをセットアップしたいディレクトリに移動
Terminalのcdコマンドでインストールさせたいディレクトリに移動。

    $ cd /***/***

###3.テンプレートプロジェクトをローカルにClone
[gulp-initial](https://github.com/kgsi/gulp-initial)をローカル環境にClone。  
SourceTree経由で構いません。Clone先は先程移動したディレクトリを指定

###4.Gulpファイルを展開・セットアップ
下記コマンドを実行。package.jsonに登録されているGulpに紐付いたツール・ライブラリが
ネットワークからダウンロードされます。

    $ sudo npm install

###5.Gulp コマンドを実行
設定されたGulpコマンドを実行してgulpを起動してみましょう。
[gulp-initial](https://github.com/kgsi/gulp-initial)の場合、
browserSyncの設定で「localhost:3000」というURLがデフォルトブラウザで自動的に起動します。

    $ gulp

--

基本的なセットアップ・操作手順は以上になります。  
なお、[gulp-initial](https://github.com/kgsi/gulp-initial)は現在も開発中で、ダウンロードのタイミングによっては仕様が変更されている可能性があるため、構築前にリポジトリのドキュメントを参照して下さい。
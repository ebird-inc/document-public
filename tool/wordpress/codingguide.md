
# WordPerss コーディングガイド
* 2015.12.15 update
* 記述者: kgsi

## HTMLフェーズ
### WordPress用HTML作成時の留意点
* リンク、パスは全てルートパスで作成する。
* 画像やCSS、JSはルートディレクトリ上にまとめて設置する。
* 構築後のメンテナンス性を考え、共通パーツはできるだけパーツ化する。  
（header.php, sidebar.php, footer.php等のインクルードパーツを活用する）  

## 実装・構築フェーズ
### 留意点
* 構築初めには、セキュリティ的な事情を鑑み、なるべく最新のバージョンをインストールする。
※ページ数が少ない場合はその限りではない
* 複雑になる場合を除き、できるだけマルチサイト化ではなく、カスタム投稿で対応できるようにする。
※余計な画面を増やすとユーザビリティが低下するため。
* Wordpressの複数インストールする必要がある場合は、なるべくマルチサイト化で対応できるようにする。
* CMS化の必要の無いページはWordpress化しない。
* 静的ページ（html）と動的ページ（Wordpres化したページ）が混在し、且つページ数が多い場合は、後々のメンテナンス性を考え、phpのインクルードを使うなどして、パーツを共通化する。
* 投稿画面に必要のない入力ブロックを残しておくとユーザビリティが低下するので、特に理由がなければ消すようにする。（タグやリンク、カスタムフィールド入力など）
* テストサイトは、同一の環境（サーバーやディレクトリ階層）で基本構築する。

### インストール
#### インストール環境
* PHP 5.2.4 以上がインストールされている。
* MySQL 5.0.15 以上がインストールされている。
* Apache mod_rewrite がサポートされている。
* phpの標準的なモジュールがインストールされている。  
（特にphp-gd5モジュールは画像投稿するために必須）

#### インストール後にまず確認すること
* htaccessによるリダイレクトが可能か(mod_writeの確認)
* 画像ファイルその他の、ファイルアップロードが可能か（パーミッションの確認）
* 画像のリサイズ編集が可能か（gd_moduleの確認）

### 管理画面について
Wordpressの管理画面は使わない機能が多くあります。
特にクライアントからするとボタンやメニューがあるだけで混乱します。使いやすいUIを提供するため、e-birdルールとして以下のカスタマイズを行ってください。

#### 作業一覧
* 不要メニューの非表示化（後述するAdmin Menu Editorを使う）
* メディアへのカテゴリー機能追加
* アップデート表示を非表示にする

### セキュリティ
* WPのログインIDは、admin以外のものを作ってadminは消す（引き渡し時に）
* wp-config.phpのパーミッションを404にする。
* htaccessのパーミッションは604に変更する（編集可能な場合）

### プラグイン
* 構築に不要なプラグインは、引き渡す前にできるだけ削除する。
* 下記のプラグインは最低限インストールする
    * 自動アップデートを無効化（[Disable All WordPress Updates](https://ja.wordpress.org/plugins/disable-wordpress-updates/)）
    * 投稿を簡単に複製できるように（[Duplicate Post](https://ja.wordpress.org/plugins/duplicate-post/)）
    * 投稿の並び替えを簡単に（[Intuitive Custom Post Order](https://ja.wordpress.org/plugins/intuitive-custom-post-order/)）
    * ライブラリにカテゴリ情報を付与（[Media Library Categories](https://ja.wordpress.org/plugins/wp-media-library-categories/)）
    * 日本語のマルチバイト処理の強化（WP Multibyte Patch）
    * 管理画面メニューのカスタマイズ（[Admin Menu Editor](https://ja.wordpress.org/plugins/admin-menu-editor/)）
* 下記のプラグインは必要に応じてインストールする
    * カスタム投稿を作成する場合（[Custom Post Type UI](https://ja.wordpress.org/plugins/custom-post-type-ui/)）
    * カスタムフィールドを作成する場合（[Advanced Custom Fields](https://ja.wordpress.org/plugins/advanced-custom-fields/)）

## 運用フェーズ
* 作成していた時のVerをアップデートすることは控える。
* アップデートがどうしても必要な場合は、新VerでWordPressのテンプレートタグが無効か有効かかならずチェックする。
